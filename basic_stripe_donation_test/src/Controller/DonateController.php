<?php
/**
 * Basic controller
 */

namespace Drupal\basic_stripe_donation_test\Controller;

use Drupal\Core\Controller\ControllerBase;

Class DonateController extends ControllerBase
{
  public function donate()
  {

      $element = array(
        '#markup' => '
          <script src="https://js.stripe.com/v3/"></script>

          <form action="/donation" method="post" id="stripe-donation-form">
            <div class="group">
              <label>
                Name
                <input name="cardholder-name" class="field" placeholder="Jane Doe" />
              </label>
              <label>
                Email
                <input class="field" placeholder="youremail@domain.com" type="email" />
              </label>
            </div>
            <div class="group">
              <label>
                Card
                <div id="card-element" class="field"></div>
              </label>
            </div>
            <button type="submit">Pay $25</button>
            <div class="outcome">
              <div class="error"></div>
              <div class="success">
                Success! Your Stripe token is <span class="token"></span>
              </div>
            </div>
          </form>
          <script>
            alert("hello");
            console.log("I am magic");


          </script>


        ',
        '#allowed_tags' => ['form', 'script', 'h2', 'label', 'div', 'input', 'button'],
      );
      return $element;

  }
}
